#! /usr/bin/env python

import rospy

import actionlib

import speech2txt_action.msg



class SpeechAction(object):
    # create messages that are used to publish feedback/result
    _feedback = speech2txt_action.msg.SpeechFeedback()
    _result = speech2txt_action.msg.SpeechResult()

    def __init__(self, name):
        self._action_name = name
        self._as = actionlib.SimpleActionServer(self._action_name, speech2txt_action.msg.SpeechAction, execute_cb=self.execute_cb, auto_start = False)
        self._as.start()
      
    def execute_cb(self, goal):
        # helper variables
        r = rospy.Rate(1)
        success = True
        
        
        # publish info to the console for the user
        rospy.loginfo('performing sentimental analysis of text %s',goal.text)
        x=goal.text
       
        # check that preempt has not been requested by the client
        if self._as.is_preempt_requested():
                rospy.loginfo('%s: Preempted' % self._action_name)
                self._as.set_preempted()
                success = False
           
            # publish the feedback
        
        #create list for sentimental analysis     
        positive=["cooperative","divine","dependable","accommodating","Blissful","Jovial","Thrilled"]
        negative=["insipid","Evil","grim","monstrous" ,"Dreadful","Apathy","Insidious","Dreadful"]
        nuetral=["clinical","objective","nostalgic","ok","hello","he","she"]
        
        for i in positive:           
           if i==x:
               self._feedback.sequence='It is a positive word'
               self._as.publish_feedback(self._feedback)
        for j in negative:           
           if j==x:
               self._feedback.sequence='It is a negative word'
               self._as.publish_feedback(self._feedback)
            # this step is not necessary, the sequence is computed at 1 Hz for demonstration purposes
        r.sleep()

        for k in nuetral:
           if k==x:
               self._feedback.sequence='It is a nuetral word'
               self._as.publish_feedback(self._feedback)
            # this step is not necessary, the sequence is computed at 1 Hz for demonstration purposes
        r.sleep()
          
        if success:
            self._result.sequence = self._feedback.sequence
            rospy.loginfo('%s: Succeeded' % self._action_name)
            self._as.set_succeeded(self._result)
        
if __name__ == '__main__':
    rospy.init_node('speech')
    server = SpeechAction(rospy.get_name())
    rospy.spin()
