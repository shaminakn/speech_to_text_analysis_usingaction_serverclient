#! /usr/bin/env python
from __future__ import print_function
import rospy

import actionlib
import speech_recognition as sr

# Brings in the messages used by the speech action, including the
# goal message and the result message.
import speech2txt_action.msg

def speech2txt_client():
    # Creates the SimpleActionClient, passing the type of the action
    client = actionlib.SimpleActionClient('speech', speech2txt_action.msg.SpeechAction)
    
    # Waits until the action server has started up and started
    # listening for goals.
    client.wait_for_server()
    
    r = sr.Recognizer()
    with sr.WavFile("audio name.wav") as source:
         audio=r.record(source)
    y=r.recognize_google(audio)

    # Creates a goal to send to the action server.
    goal = speech2txt_action.msg.SpeechGoal(text=y)

    #feedback=speech2txt_action.msg.SpeechFeedback(sequence=y)
    #client.send_feedback(feedback)
    client.send_goal(goal)
    

    # Waits for the server to finish performing the action.
    client.wait_for_result()

    # Prints out the result of executing the action
    return client.get_result() 

if __name__ == '__main__':
    try:
        # Initializes a rospy node 
        rospy.init_node('speech_client')
        result = speech2txt_client()
        print("Result:",result.sequence)
    except rospy.ROSInterruptException:
        print("program interrupted before completion", file=sys.stderr)
